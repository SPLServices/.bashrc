# Use specific aliases and functions
#Generic
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ls='ls -abp --color=auto'
alias grep='grep --color=auto'

#Utility Servers
alias deps='ssh splunk@IPofDEPS'
alias lms='ssh splunk@IPofLMS'
alias cms='ssh splunk@IPofCMS'
alias dss='ssh splunk@IPofDS'
alias hfs='ssh splunk@IPofHF'

#Search Heads
alias sh1='ssh splunk@IPofSH1'
alias sh2='ssh splunk@IPofSH2'

#Indexers
alias idx1='ssh splunk@IPofIDX1'
alias idx2='ssh splunk@IPofIDX2'

#Splunk Specific
alias sapp='cd /opt/splunk/etc/apps/'
alias syslocal='cd /opt/splunk/etc/system/local/'
alias dapp='cd /opt/splunk/etc/deployment-apps/'
alias stail='tail -f /opt/splunk/var/log/splunk/splunkd.log'
alias staile='tail -f /opt/splunk/var/log/splunk/splunkd.log | grep -v INFO'
alias mapp='cd /opt/splunk/etc/master-apps/'
alias slapp='cd /opt/splunk/etc/slave-apps/'
alias shcapp='cd /opt/splunk/etc/shcluster/apps'
alias reload='/opt/splunk/bin/splunk reload deploy-server'
alias splkps='ps -fu splunk'
alias splknw='netstat -an | grep LISTEN'
source /opt/splunk/bin/setSplunkEnv 

# Source global definitions
if [ -f /etc/bashrc ]; then
. /etc/bashrc
fi

#Visual & Editors
export PS1='\[\033[31;1m[\u@\h]\[\033[32;1m\w\[\033[34;1m\$ \[\033[0m'
export PATH=/usr/local/bin:/usr/sbin:/usr/ucb/bin:/usr/local/sbin:$PATH
export EDITOR=vi
